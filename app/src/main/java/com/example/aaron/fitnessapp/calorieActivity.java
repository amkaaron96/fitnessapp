package com.example.aaron.fitnessapp;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class calorieActivity extends Activity implements View.OnClickListener{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calorie);
        findViewById(R.id.calcCalories).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.calcCalories:
                runCalc();
                break;
        }
    }

    public void runCalc(){
        int calories;
        int totalCalories;

        EditText cals = findViewById(R.id.caloricValue);
        TextView showCals = findViewById(R.id.totalCalories);

        totalCalories = Integer.parseInt(showCals.getText().toString());
        calories = Integer.parseInt(cals.getText().toString());

        int newCals = totalCalories + calories;

        showCals.setText(String.valueOf(newCals));
    }
}
