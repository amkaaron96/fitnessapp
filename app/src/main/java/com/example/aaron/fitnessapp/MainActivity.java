package com.example.aaron.fitnessapp;

import android.app.Activity;
import android.view.View.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends Activity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.heartrate).setOnClickListener(this);
        findViewById(R.id.bmi).setOnClickListener(this);
        findViewById(R.id.calorie).setOnClickListener(this);
        findViewById(R.id.water).setOnClickListener(this);
        findViewById(R.id.workouts).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.heartrate:
                Intent heartrateIntent = new Intent(this, heartrateActivity.class);
                startActivity(heartrateIntent);
                break;
            case R.id.bmi:
                Intent bmiIntent = new Intent(this, bmiActivity.class);
                startActivity(bmiIntent);
                break;
            case R.id.calorie:
                Intent calorieIntent = new Intent(this, calorieActivity.class);
                startActivity(calorieIntent);
                break;
            case R.id.water:
                Intent waterIntent = new Intent(this, waterActivity.class);
                startActivity(waterIntent);
                break;
            case R.id.workouts:
                Intent workoutsIntent = new Intent(this, workoutsActivity.class);
                startActivity(workoutsIntent);
                break;
        }
    }
}
