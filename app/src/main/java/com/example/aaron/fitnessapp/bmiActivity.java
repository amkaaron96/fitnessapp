package com.example.aaron.fitnessapp;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class bmiActivity extends Activity implements View.OnClickListener{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bmi);
        findViewById(R.id.calcBmi).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.calcBmi:
                runCalc();
                break;
        }
    }

    public void runCalc(){
        String height;
        String weight;
        EditText heightValue = findViewById(R.id.userHeight);
        EditText weightValue = findViewById(R.id.userWeight);
        TextView bmiValue = findViewById(R.id.bmiValue);
        TextView bmiConsider = findViewById(R.id.bmiConsider);

        height = heightValue.getText().toString();
        weight = weightValue.getText().toString();

        int intHeight = Integer.parseInt(height);
        int intWeight = Integer.parseInt(weight);

        double bmi = ((intWeight * 0.45) / ((intHeight * 0.025) * (intHeight * 0.025)));

        String considered = "Could not find chart value, check your inputs!";

        if(bmi < 19){
            considered = "Underweight";
        }
        if(bmi >= 19 && bmi < 25){
            considered = "Healthy";
        }
        if(bmi >= 25 && bmi < 30){
            considered = "Overweight";
        }
        if(bmi >= 30 && bmi < 40){
            considered = "Obese";
        }
        if(bmi >= 40 && bmi < 51){
            considered = "Extremely Obese";
        }

        bmiValue.setText(String.format("%.1f", bmi));
        bmiConsider.setText(considered);
    }
}
