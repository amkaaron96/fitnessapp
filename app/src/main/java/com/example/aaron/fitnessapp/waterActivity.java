package com.example.aaron.fitnessapp;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class waterActivity extends Activity implements View.OnClickListener{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_water);
        findViewById(R.id.waterButton).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.waterButton:
                runCalc();
                break;
        }
    }

    public void runCalc(){
        int waterValue;
        EditText userWater = findViewById(R.id.waterValue);
        TextView waterMessage = findViewById(R.id.water);

        waterValue = Integer.parseInt(userWater.getText().toString());
        int ozRemain = (64 - waterValue);
        double ltRemain = (ozRemain * 0.0295735);
        double cupsRemain = (ozRemain * 0.125);

        String message = (ozRemain + " ounces, or " + String.format("%.1f", ltRemain) + " liters, or " + String.format("%.1f", cupsRemain) + " cups.");

        waterMessage.setText(message);
    }
}