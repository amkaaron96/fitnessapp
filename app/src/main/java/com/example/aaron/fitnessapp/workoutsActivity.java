package com.example.aaron.fitnessapp;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class workoutsActivity extends Activity implements View.OnClickListener{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workouts);
        findViewById(R.id.workoutsButton).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.workoutsButton:
                runCalc();
                break;
        }
    }

    public void runCalc(){
        String workout;

        TextView workoutView = findViewById(R.id.workouts);
        EditText name = findViewById(R.id.name);
        EditText desc = findViewById(R.id.desc);

        workout = workoutView.getText().toString();

        workout = workout + name.getText().toString() + "\n" + desc.getText().toString() + "\n" + "\n";

        workoutView.setText(workout);
    }
}