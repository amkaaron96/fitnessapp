package com.example.aaron.fitnessapp;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

public class heartrateActivity extends Activity implements View.OnClickListener{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_heartrate);
        findViewById(R.id.calcHeartrate).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.calcHeartrate:
                runCalc();
                break;
        }
    }

    public void runCalc(){
        int heartrate;
        int age;

        EditText userAge = findViewById(R.id.age);
        TextView showHeartrate = findViewById(R.id.heartrate);

        age = Integer.parseInt(userAge.getText().toString());

        heartrate = (220 - age);

        showHeartrate.setText(String.valueOf(heartrate));
    }
}
